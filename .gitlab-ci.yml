variables:
  GITLAB_PASTEUR_PROJECT_ID: 6531
  PROJECT_NAME: nyxui

stages:
  - build
  - deploy

.build-with-buildah:
  # see https://dsi-docs.pasteur.cloud/docs/gitlab/cicd/buildah/
  stage: build
  image: quay.io/buildah/stable:v1.37
  variables:
    STORAGE_DRIVER: vfs
    BUILDAH_FORMAT: docker
    BUILDAH_ISOLATION: chroot
    FQ_IMAGE_NAME: "$CI_REGISTRY_IMAGE/front:$CI_COMMIT_SHORT_SHA"
    BUILD_CONTEXT: "."
    CONTAINERFILE: "front/Containerfile"
  before_script:
    - export REGISTRY_AUTH_FILE="$HOME/auth.json"
    - echo "$CI_REGISTRY_PASSWORD" | buildah login -u "$CI_REGISTRY_USER" --password-stdin "$CI_REGISTRY"
  script:
    - buildah build -t "$FQ_IMAGE_NAME" -f "$CONTAINERFILE" "$BUILD_CONTEXT"
    - buildah push "$FQ_IMAGE_NAME"

build on gitlab.pasteur.fr:
  extends: .build-with-buildah
  variables:
    PUBLIC_URL: "nyx.pasteur.cloud"
  rules:
    - if: ($CI_COMMIT_BRANCH == "main" &&
        $CI_PROJECT_ID == $GITLAB_PASTEUR_PROJECT_ID)      # gitlab.pasteur.fr only

build dev on gitlab.pasteur.fr:
  extends: .build-with-buildah
  variables:
    PUBLIC_URL: "nyx.dev.pasteur.cloud"
  rules:
    - if: ($CI_COMMIT_BRANCH == "dev" &&
        $CI_PROJECT_ID == $GITLAB_PASTEUR_PROJECT_ID)      # gitlab.pasteur.fr only

.deploy-with-manifests:
  stage: deploy
  image: docker.io/enix/ci-toolbox:1.21
  variables:
    APP_NAME: nyxui
    FQ_IMAGE_NAME: "$CI_REGISTRY_IMAGE/front:$CI_COMMIT_SHORT_SHA"
  script:
    - kubectl create secret
      docker-registry registry-gitlab
      --docker-server=registry-gitlab.pasteur.fr
      --docker-username="$DOCKER_USER"
      --docker-password="$DOCKER_TOKEN"
      --docker-email=kubernetes@pasteur.fr
      -n "$KUBE_NAMESPACE"
      --dry-run=client -o yaml | kubectl apply -f -
    - envsubst < k8s/front-deployment.yaml | kubectl apply -n "$KUBE_NAMESPACE" -f -
    - envsubst < k8s/front-service.yaml | kubectl apply -n "$KUBE_NAMESPACE" -f -
    - envsubst < k8s/ingress.yaml | kubectl apply -n "$KUBE_NAMESPACE" -f -

.deploy-with-helm:
  stage: deploy
  image: docker.io/enix/ci-toolbox:1.21
  variables:
    IMAGE_NAME: "front"
    IMAGE_POLICY: "Always"
  script:
    - kubectl create secret
      docker-registry registry-gitlab
      --docker-server=registry-gitlab.pasteur.fr
      --docker-username="$DOCKER_USER"
      --docker-password="$DOCKER_TOKEN"
      --docker-email=kubernetes@pasteur.fr
      --dry-run=client
      -o yaml | kubectl apply -f -
    - helmfile lint
    - helmfile template
    - helmfile sync

deploy to pasteur.cloud:
  extends: .deploy-with-helm
  variables:
    SERVICE_TARGET_PORT: "8080"
    IMAGE_SECRETS: "registry-gitlab"
    KUBE_NAMESPACE: "nyx-prod"
    PUBLIC_URL: "nyx.pasteur.cloud"
    INGRESS_CLASS: "external"
    INGRESS_URL: "nyx.pasteur.cloud"
    SERVICE_PORT: "80"
    LIMITS_CPU: "2"
    LIMITS_MEMORY: "4Gi"
    LIMITS_STORAGE: "1Gi"
  environment:
    name: k8sprod-02/nyx-prod
    url: https://nyx.pasteur.cloud
  rules:
    - if: ($CI_COMMIT_BRANCH == "main" &&
        $CI_PROJECT_ID == $GITLAB_PASTEUR_PROJECT_ID)      # gitlab.pasteur.fr only
      when: manual

deploy to dev.pasteur.cloud:
  extends: .deploy-with-helm
  variables:
    SERVICE_TARGET_PORT: "8080"
    IMAGE_SECRETS: "registry-gitlab"
    KUBE_NAMESPACE: "nyx-dev"
    PUBLIC_URL: "nyx.dev.pasteur.cloud"
    INGRESS_CLASS: "internal"
    INGRESS_URL: "nyx.dev.pasteur.cloud"
    SERVICE_PORT: "80"
    LIMITS_CPU: "2"
    LIMITS_MEMORY: "4Gi"
    LIMITS_STORAGE: "1Gi"
  environment:
    name: k8sdev-01/nyx-dev
    url: https://nyx.dev.pasteur.cloud
  rules:
    - if: ($CI_COMMIT_BRANCH == "dev" &&
        $CI_PROJECT_ID == $GITLAB_PASTEUR_PROJECT_ID)      # gitlab.pasteur.fr only
      when: manual

