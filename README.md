The Nyx project focuses on the neural activity and locomotion of the *Drosophila* larva.

# NyxUI.jl

Web interface meant to be served at [nyx.pasteur.cloud](https://nyx.pasteur.cloud/) (public) and [nyx.dev.pasteur.cloud](https://nyx.dev.pasteur.cloud/) (internal).

It features an app catalog, with the following apps:

* an editor for muscular activity programs,
* [LarvaTagger.jl](https://gitlab.pasteur.fr/nyx/larvatagger.jl) without any automating tagging backends.


## Local installation

For local execution, the installation instructions are as follows:

You will need [JuliaUp](https://github.com/JuliaLang/juliaup?tab=readme-ov-file#juliaup---julia-version-manager) and [git](https://git-scm.com/downloads).

Once these tools are installed, in a terminal (on Windows, preferably PowerShell), type:
```
git clone --branch dev https://gitlab.pasteur.fr/nyx/NyxUI.jl NyxUI
cd NyxUI
cp front/Manifest.toml .
juliaup add lts
juliaup default lts
julia --project=. -e 'using Pkg; Pkg.instantiate()'
julia --project=. routes.jl
```

You may be asked whether to authorize ports 9284 and 9285; please give the app permission.

From there, in a web browser, open http://localhost:9284/ to access the app.

Note that the generated files can be found somewhere in directory *storage/exports*.

The download buttons in the LarvaTagger app will not work in the web browser.
To make them work, the reverse proxy setup in the container image called *front* is required.
If you have [Podman](https://podman.io/), as an alternative to the above installation procedure, you can simply run `front/build.sh --now` and, once the container is up and running, connect to http://localhost:8080/.
