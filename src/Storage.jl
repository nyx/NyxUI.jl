module Storage

using Dates
using TOML

export getconfig, getbucket, clear_oldest, purge_appdata

const __storage_location__ = get(ENV, "STORAGE",
                                 joinpath(dirname(Base.current_project()), "storage"))

const __config_file__ = get(ENV, "CONFIGFILE",
                            joinpath(dirname(Base.current_project()), "Config.toml"))

const __config__ = Ref{Union{Nothing, Dict{String, Any}}}(nothing)

function getconfig()
    if isnothing(__config__[])
        __config__[] = TOML.parsefile(__config_file__)
    end
    return __config__[]
end

function getconfig(key, morekeys...; default=nothing)
    config = getconfig()
    i = 0
    while key in keys(config)
        config = config[key]
        i += 1
        i <= length(morekeys) || break
        key = morekeys[i]
    end
    config isa Dict ? default : config
end

function getbucket(session_id, appname)
    joinpath(__storage_location__, "exports", session_id, appname)
end

function getbucket(session_id, appname, mode)
    session_bucket = getbucket(session_id, appname)
    if mode === :read
        session_bucket
    elseif mode === :write
        date_time = Dates.format(now(), "yyyymmdd_HHMMSS")
        newdir = joinpath(session_bucket, date_time)
        mkpath(newdir)
        newdir
    else
        throw("`mode` not in [:read, :write]: mode=$mode")
    end
end

function getbucket(; child_resource)
    parts = splitpath(child_resource)
    exports = findfirst(==("exports"), parts)
    session_id = parts[exports + 1]
    appname = parts[exports + 2]
    return getbucket(session_id, appname)
end

function clear_oldest(session_bucket)
    oldest = first(sort(readdir(session_bucket; join=false)))
    oldest = joinpath(session_bucket, oldest)
    @info "Deleting directory" dir=oldest
    rm(oldest; recursive=true)
end

function purge_appdata(session_id, appname)
    bucket = getbucket(session_id, appname, :read)
    isdir(bucket) && rm(bucket; recursive=true)
    exportdir = joinpath("public", session_id, appname)
    isdir(exportdir) && rm(exportdir; recursive=true)
end

end
