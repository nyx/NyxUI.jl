module AppCatalog

using Genie, Stipple, StippleUI
import Stipple: @app, @init
using LazyArtifacts

export app_catalog_view

const project_root = dirname(Base.current_project())

@app begin
end

function app_catalog_view()
    col = "display:flex;flex-direction:column;"
    row = "display:flex;flex-direction:row;"
    img = "width:492px;height:384px;"
    caption = "absolute-bottom text-subtitle1 text-center"
    header1 = "color:var(--q-color-secondary);margin:0;text-align:center;"
    header2 = "color:var(--q-color-primary);text-align:center;"
    app = "app"

    Html.div(style=col, [
        Html.h2("Welcome to Nyx app catalog!", class="banner1",
            style=header1), # fight against stipple-core
        Html.div(class="banner2", style=row, [
                item(item(class=app, Html.a(href=Router.link_to(:get_muscles), [
                Html.h1("Muscle app", style=header2),
                imageview(src="/images/MuscleApp.png", style=img),
            ]))),
            item(item(class=app, Html.a(href="", class="disabled", [
                Html.h1("3D simulation", style=header2),
                imageview(src="/images/3DSimulation.png", style=img,
                    Html.div(class=caption, "Coming soon!"),
                ),
            ]))),
            item(item(class=app, Html.a(href=Router.link_to(:get_larvatagger), [
                Html.h1("LarvaTagger", style=header2),
                imageview(src="/images/LarvaTagger.png", style=img,
                    Html.div(class=caption, "Partially available"),
                ),
            ]))),
        ]),
    ])
end

const ui = app_catalog_view

function handler(modelview)
    Stipple.Layout.add_css("/css/appcatalog.css")
    publish_images()
    empty_model = @init
    modelview(empty_model, app_catalog_view)
end

function publish_images()
    srcdir = joinpath(artifact"catalog_images", "images")
    dstdir = joinpath(project_root, "public", "images")
    mkpath(dstdir)
    for file in readdir(srcdir, join=false)
        srcfile = joinpath(srcdir, file)
        dstfile = joinpath(dstdir, file)
        if !isfile(dstfile) # assume the file has not changed
            open(srcfile, "r") do i
                open(dstfile, "w") do o
                    write(o, read(i))
                end
            end
        end
    end
    return dstdir
end

end
