module MuscleWidgets

using NyxWidgets.Players, NyxWidgets.Muscles, NyxWidgets.AnimatedLayers
import NyxWidgets.Base: lowerdom, dom_id, dom_id!, Div
using NyxPlots
using Observables
using Bonito
using Colors
using ColorSchemes

export MuscleWidget

const NyxCSS = Bonito.Asset(joinpath(@__DIR__, "bonito.css"))

include("colorscales.jl")

const __default_colormap__ = "Viridis"

const __colormaps__ = collect(keys(__plotly_colorschemes__))

struct MuscleWidget
    sequence::AbstractObservable
    colormap::AbstractObservable
    colorscheme::AbstractObservable
    overview::Muscles.MultiSegmentMuscleWidget
    individualsegments::Vector{Muscles.MuscleWidget}
    heatmaps::Vector{Heatmap}
    animation::LayeredAnimation
    editmode
end

function MuscleWidget(sequence)
    if !(sequence isa AbstractObservable)
        sequence = Observable(sequence)
    end
    colormap = Observable(__default_colormap__)
    colorscheme = Observable{ColorScheme}(scale2scheme(colormap[]))
    on(colormap) do c
        colorscheme[] = scale2scheme(c)
    end
    editmode = Observable(false)
    #
    overview = Muscles.MultiSegmentMuscleWidget(; musclelabel=false, sidelabel=:right)
    individualsegments = [Muscles.MuscleWidget(; segment=segment.segment,
                                               side=segment.side,
                                               upsidedown=segment.side=="right",
                                               segmentlabel=true,
                                               style="width: 40%; margin-top: 2rem;")
                          for segment in segments(overview)]
    moveto = clipboard = zlim = setvalue = nothing
    heatmaps = Heatmap[]
    for segment in individualsegments
        halfsegment = sequence[][segment.segment, segment.side]
        heatmap = Heatmap(convert(Matrix, halfsegment),
                          halfsegment.times,
                          halfsegment.muscles;
                          colorscale=colormap,
                          width=606, height=800,
                          pick=moveto,
                          clipboard=clipboard,
                          setvalue=setvalue,
                          zlim=zlim,
                          prompt_on_assign=true)
        moveto = heatmap.pick
        clipboard = heatmap.clipboard
        zlim = heatmap.zlim
        setvalue = heatmap.setvalue
        push!(heatmaps, heatmap)
    end
    detailedviews = [Div(widget, heatmap; style=:row)
                     for (widget, heatmap) in zip(individualsegments, heatmaps)]
    times = sequence[].times
    playbackspeeds = ["0.001", "0.01", "0.1", "0.25", "0.5", "1.0"]
    animation = LayeredAnimation(times, overview, detailedviews...;
                                 playbackspeeds=playbackspeeds, bind=false)
    on(sequence) do seq
        timestamps(animation)[] = seq.times
        if timestep(animation)[] == 0
            timestep(animation)[] = min(animation)
        end
        for (segment, heatmap) in zip(individualsegments, heatmaps)
            halfsegment = seq[segment.segment, segment.side]
            Heatmaps.update(heatmap, convert(Matrix, halfsegment); x=halfsegment.times)
        end
    end
    on(Muscles.click(Muscles.MuscleWidget, overview)) do activesegment
        @info "Click on segment" name=activesegment.segment side=activesegment.side
        for (container, segment) in zip(detailedviews, individualsegments)
            if segment.segment == activesegment.segment &&
                segment.side == activesegment.side
                if container !== activelayer(animation)
                    open(animation, container)
                end
                break
            end
        end
    end
    on(animation) do step
        @assert step isa Int
        layer = activelayer(Int, animation)
        widget = layer == 1 ? overview : individualsegments[layer - 1]
        if step == 0
            clear(widget)
        else
            drawframe(widget, sequence[], step, colorscheme[])
        end
    end
    on(Players.speed(animation)) do s
        @info "Playback speed" val=s
    end
    on(moveto) do xyz
        t = xyz[1]
        step = findfirst(==(t), sequence[].times)
        timestep(animation)[] = step
        @info "Click on muscle" name=xyz[2] step value=xyz[3]
    end
    for (segment, heatmap) in zip(individualsegments, heatmaps)
        onany(heatmap.assign, heatmap.paste) do _, _
            seq = sequence[][segment.segment, segment.side]
            @debug "heatmap" heatmap.z[]
            seq.activity = heatmap.z[]
        end
    end
    on(colormap) do colorscale
        @info "Colormap" colorscale
        notify(timestep(animation))
    end
    on(animation.switchlayer) do layers
        current, _, _ = layers
        if 1 < current
            heatmaps[current-1].editing[] = editmode[]
        end
    end
    on(editmode) do b
        layer = activelayer(Int, animation)
        if 1 < layer
            heatmaps[layer-1].editing[] = b
        end
    end
    return MuscleWidget(sequence, colormap, colorscheme, overview,
                        individualsegments, heatmaps, animation, editmode)
end

dom_id(widget::MuscleWidget) = dom_id(widget.animation)

dom_id!(widget::MuscleWidget, val) = dom_id!(widget.animation, val)

function Bonito.jsrender(session::Session, widget::MuscleWidget)
    Bonito.jsrender(session, NyxCSS)
    node = Bonito.jsrender(session, DOM.div(widget.animation))
    Bonito.on_document_load(session, js"""
    const lib = function() {
        let muscles = new Object(),
            musclewidget,
            heatmap;

        function addHoverListeners(parent) {
            heatmap = parent.querySelector(':scope > div > div.js-plotly-plot'),
            musclewidget = parent.querySelector(':scope > div.nyx-segment > svg');
            musclewidget.querySelectorAll(':scope > g').forEach(g => {
                let shortname = g.querySelector('text').innerHTML;
                muscles[shortname] = g;
            });
            heatmap.on('plotly_hover', data => {
                data.points.map(point => {
                    let shortname = point.y,
                        muscle = muscles[shortname];
                    musclewidget.appendChild(muscle);
                    muscle.classList.add('hovered');
                });
            }).on('plotly_unhover', data => {
                data.points.map(point => {
                    let shortname = point.y,
                        muscle = muscles[shortname];
                    muscle.classList.remove('hovered');
                });
            });
        }

        return {
            addHoverListeners,
        }
    };
    let segmenthalves = document.querySelectorAll('.nyx-animated-layers > div:not(.nyx-segment-halves)')
    segmenthalves.forEach(parent => { lib().addHoverListeners(parent) });
    """)
    # restore state on page reload (Ctrl+R)
    notify(widget.colormap)
    return node
end

function drawframe(widget, sequence, t, colorscheme)
    if widget isa Muscles.MuscleWidget
        sequence = sequence[widget]
    end
    if colorscheme isa Symbol
        colorscheme = ColorSchemes.colorschemes[colorscheme]
    end
    @debug "Drawing frame" t
    for (muscle, mesh) in muscles(widget)
        style = Muscles.polygon(mesh).style
        activation = sequence[muscle, t]
        if isnan(activation)
            if !isempty(style[])
                empty!(style[])
                notify(style)
            end
        else
            color = "#" * hex(get(colorscheme, activation))
            style[]["fill"] = color
            style[]["fill-opacity"] = 1
            notify(style)
        end
    end
end

function clear(widget)
    for (_, mesh) in muscles(widget)
        style = Muscles.polygon(mesh).style
        if !isempty(style[])
            empty!(style[])
            notify(style)
        end
    end
end

clipboard(widget::MuscleWidget) = clipboard(widget.heatmaps[1])

clipboard(heatmap::Heatmap) = heatmap.clipboard

function areaselect(widget::MuscleWidget)
    obs = Observable(true)
    f(values...) = notify(obs)
    onany(f, [h.select for h in widget.heatmaps]...)
    return obs
end

end
