
# adapted from
# https://github.com/plotly/plotly.js/blob/3eccac038dd7bff946f732613c65b094270376c2/src/components/colorscale/scales.js
const __plotly_colorschemes__ = Dict(
    "Greys" => ColorScheme([
        RGB(0, 0, 0),
        RGB(1, 1, 1),
    ]),
    "YlGnBu" => ColorScheme([
        RGB(8/255, 29/255, 88/255),
        RGB(37/255, 52/255, 148/255),
        RGB(34/255, 94/255, 168/255),
        RGB(29/255, 145/255, 192/255),
        RGB(65/255, 182/255, 196/255),
        RGB(127/255, 205/255, 187/255),
        RGB(199/255, 233/255, 180/255),
        RGB(237/255, 248/255, 217/255),
        RGB(1, 1, 217/255),
    ]),
    "Greens" => ColorScheme([
        RGB(0, 68/255, 27/255),
        RGB(0, 109/255, 44/255),
        RGB(35/255, 139/255, 69/255),
        RGB(65/255, 171/255, 93/255),
        RGB(116/255, 196/255, 118/255),
        RGB(161/255, 217/255, 155/255),
        RGB(199/255, 233/255, 192/255),
        RGB(229/255, 245/255, 224/255),
        RGB(247/255, 252/255, 245/255),
    ]),
    "YlOrRd" => ColorScheme([
        RGB(128/255, 0, 38/255),
        RGB(189/255, 0, 38/255),
        RGB(227/255, 26/255, 28/255),
        RGB(252/255, 78/255, 42/255),
        RGB(253/255, 141/255, 60/255),
        RGB(254/255, 178/255, 76/255),
        RGB(254/255, 217/255, 118/255),
        RGB(1, 237/255, 160/255),
        RGB(1, 1, 204/255),
    ]),
    "Bluered" => ColorScheme([
        RGB(0, 0, 1),
        RGB(1, 0, 0),
    ]),
    "RdBu" => ColorScheme([
        range(
            RGB(5/255, 10/255, 172/255),
            RGB(106/255, 137/255, 247/255),
            length=8,
        )...,
        range(
            RGB(106/255, 137/255, 247/255),
            RGB(190/255, 190/255, 190/255),
            length=4,
        )[2:end-1]...,
        range(
            RGB(190/255, 190/255, 190/255),
            RGB(220/255, 170/255, 132/255),
            length=3,
        )...,
        range(
            RGB(220/255, 170/255, 132/255),
            RGB(230/255, 145/255, 90/255),
            length=3,
        )[2],
        range(
            RGB(230/255, 145/255, 90/255),
            RGB(178/255, 10/255, 28/255),
            length=7,
        )...,
    ]),
    "Reds" => ColorScheme([
        RGB(220/255, 220/255, 220/255),
        RGB(245/255, 195/255, 157/255),
        range(
            RGB(245/255, 160/255, 105/255),
            RGB(178/255, 10/255, 28/255),
            length=4,
        )...,
    ]),
    "Blues" => ColorScheme([
        range(
            RGB(5/255, 10/255, 172/255),
            RGB(40/255, 60/255, 190/255),
            length=8,
        )...,
        range(
            RGB(40/255, 60/255, 190/255),
            RGB(70/255, 100/255, 245/255),
            length=4,
        )[2:end-1]...,
        range(
            RGB(70/255, 100/255, 245/255),
            RGB(90/255, 120/255, 245/255),
            length=3,
        )...,
        range(
            RGB(90/255, 120/255, 245/255),
            RGB(106/255, 137/255, 247/255),
            length=3,
        )[2],
        range(
            RGB(106/255, 137/255, 247/255),
            RGB(220/255, 220/255, 220/255),
            length=7,
        )...,
    ]),
    "Picnic" => ColorScheme([
        RGB(0, 0, 1),
        RGB(51/255, 153/255, 1),
        RGB(102/255, 204/255, 1),
        RGB(153/255, 204/255, 1),
        RGB(204/255, 204/255, 1),
        RGB(1, 1, 1),
        RGB(1, 204/255, 1),
        RGB(1, 153/255, 1),
        RGB(1, 102/255, 204/255),
        RGB(1, 102/255, 102/255),
        RGB(1, 0, 0),
    ]),
    "Rainbow" => ColorScheme([
        RGB(150/255, 0, 90/255),
        RGB(0, 0, 200/255),
        RGB(0, 25/255, 1),
        RGB(0, 152/255, 1),
        RGB(44/255, 1, 150/255),
        RGB(151/255, 1, 0),
        RGB(1, 234/255, 0),
        RGB(1, 111/255, 0),
        RGB(1, 0, 0),
    ]),
    "Portland" => ColorScheme([
        RGB(12/255, 51/255, 131/255),
        RGB(10/255, 136/255, 186/255),
        RGB(242/255, 211/255, 56/255),
        RGB(242/255, 143/255, 56/255),
        RGB(217/255, 30/255, 30/255),
    ]),
    "Jet" => ColorScheme([
        RGB(0, 0, 131/255),
        range(
            RGB(0, 60/255, 170/255),
            RGB(5/255, 1, 1),
            length=3,
        )...,
        range(
            RGB(5/255, 1, 1),
            RGB(1, 1, 0),
            length=3,
        )[2],
        range(
            RGB(1, 1, 0),
            RGB(250/255, 0, 0),
            length=3,
        )...,
        RGB(128/255, 0, 0),
    ]),
    "Hot" => ColorScheme([
        range(
            RGB(0, 0, 0),
            RGB(230/255, 0 ,0),
            length=4,
        )...,
        range(
            RGB(230/255, 0 ,0),
            RGB(1, 210/255, 0),
            length=4,
        )[2:end-1]...,
        range(
            RGB(1, 210/255, 0),
            RGB(1 ,1, 1),
            length=5,
        )...,
    ]),
    "Blackbody" => ColorScheme([
        range(
            RGB(0, 0, 0),
            RGB(230/255, 0, 0),
            length=3,
        )...,
        range(
            RGB(230/255, 0, 0),
            RGB(230/255, 210/255, 0),
            length=3,
        )[2],
        range(
            RGB(230/255, 210/255, 0),
            RGB(1, 1, 1),
            length=4,
        )...,
        range(
            RGB(1, 1, 1),
            RGB(160/255, 200/255, 1),
            length=4,
        )[2:end]...,
    ]),
    "Earth" => ColorScheme([
        RGB(0, 0, 130/255),
        RGB(0, 180/255, 180/255),
        range(
            RGB(40/255, 210/255, 40/255),
            RGB(230/255, 230/255, 50/255),
            length=3,
        )...,
        range(
            RGB(230/255, 230/255, 50/255),
            RGB(120/255, 70/255, 20/255),
            length=3,
        )[2],
        range(
            RGB(120/255, 70/255, 20/255),
            RGB(1, 1, 1),
            length=5,
        )...,
    ]),
    "Electric" => ColorScheme([
        range(
            RGB(0, 0, 0),
            RGB(30/255, 0, 100/255),
            length=4,
        )...,
        range(
            RGB(30/255, 0, 100/255),
            RGB(120/255, 0, 100/255),
            length=6,
        )[2:end-1]...,
        range(
            RGB(120/255, 0, 100/255),
            RGB(160/255, 90/255, 0),
            length=5,
        )...,
        range(
            RGB(160/255, 90/255, 0),
            RGB(230/255, 200/255, 0),
            length=5,
        )[2:end-1]...,
        range(
            RGB(230/255, 200/255, 0),
            RGB(1, 250/255, 220/255),
            length=5,
        )...,
    ]),
)

function scale2scheme(colorscale)
    if haskey(__plotly_colorschemes__, colorscale)
        colorscheme = __plotly_colorschemes__[colorscale]
    else
        rev = colorscale in ("Greys", "YlGnBu", "Greens", "YlOrRd", "RdBu", "Blues")
        cs = if colorscale in ("Earth", ) # "Rainbox"?
            Symbol("gist_$(lowercase(colorscale))")
        elseif colorscale in ("Rainbow", "Hot", "Blackbody", "Viridis", "Cividis")
            Symbol(lowercase(colorscale))
        elseif colorscale in ("Jet", )
            Symbol(lowercase(colorscale) * "1")
        else
            Symbol(colorscale)
        end
        colorscheme = ColorSchemes.colorschemes[cs]
        if rev
            colorscheme = reverse(colorscheme)
        end
    end
    return colorscheme
end
