module Backbone

using NyxWidgets.Base: Cache
using NyxUI.MuscleActivities
using NyxUI.Storage
using Bonito

include("MuscleWidgets.jl")
using .MuscleWidgets

export hasmodel, getmodel, setmodel, newsequence, getsequence, withsequences,
       exportsequence, loadsequence, deletesequence, purgesession

# mutable struct Backbone{W}
#     widget::Union{Nothing, W}
#     channel::Observable{Bonito.JSCode}
# end
#
# Backbone{W}() where {W} = Backbone{W}(nothing)
#
# Backbone{W}(widget::W) where {W} = Backbone{W}(widget, Observable(js""))
#
# Backbone(widget::W) where {W} = Backbone{W}(widget)
#
# function Bonito.jsrender(session::Session, widget::Backbone)
#     isnothing(widget.widget) && error("backbone widget not initialized")
#     on(session, widget.channel) do jscode
#         evaljs(session, jscode)
#     end
#     Bonito.jsrender(session, widget.widget)
# end

## models

const __backbone_cache__ = Cache{String, MuscleWidget}()
const __valid_sessions__ = Dict{String, Bool}()

function app(persistent=""; title="Nyx muscle activity")
    App(; title=title) do session
        # # embedded apps are always renderded twice; save time # EDIT: except on Ctrl+R
        # valid = get(__valid_sessions__, persistent, false)
        # __valid_sessions__[persistent] = !valid
        # valid || return
        @info "New session" session.id
        cache_id = isempty(persistent) ? session.id : persistent
        model = __backbone_cache__[cache_id]
        Bonito.jsrender(session, model)
    end
end

## muscle model

getmodel(session_id) = __backbone_cache__[session_id]#.widget

# for debugging
getmodel() = first(values(__backbone_cache__))#.widget

hasmodel(session_id) = haskey(__backbone_cache__, session_id)

function setmodel(session_id, sequence)
    __backbone_cache__[session_id] = MuscleWidget(sequence)
end

# getchannel(session_id) = __backbone_cache__[session_id].channel

## sequence model

function newsequencename(stem, existing)
    if stem == "Empty"
        stem = "New"
    end
    name = stem
    i = 0
    if stem in ("New", "Random")
        i = 1
        name = "$stem ($i)"
    end
    while name in existing
        i += 1
        name = "$stem ($i)"
    end
    return name
end

function newsequence(session_id, sequence_name, sequence_names, start_time, time_interval,
        series_length)
    name = newsequencename(sequence_name, sequence_names)
    stop_time = start_time + (series_length-1) * time_interval
    ts = start_time:time_interval:stop_time
    model = getmodel(session_id)
    sequence = MuscleActivity(name, ts, model.overview)
    if sequence_name == "Random"
        MuscleActivities.rand!(sequence)
    end
    addsequence(session_id, sequence)
    model.sequence[] = sequence
    return sequence
end

const __sequences__ = Cache{String, Cache{Int, MuscleActivity}}()

function getsequence(session_id, sequence_name)
    model = getmodel(session_id)
    sequence = nothing
    if model.sequence[].program_name != sequence_name
        lock(__sequences__[session_id]) do
            for outer sequence in values(__sequences__[session_id])
                sequence.program_name == sequence_name && break
            end
        end
        model.sequence[] = sequence
    end
    return sequence
end

getsequence(session_id) = getmodel(session_id).sequence[]

function addsequence(session_id, sequence)
    lock(__sequences__[session_id]) do
        sequences = __sequences__[session_id]
        i = isempty(sequences) ? 0 : maximum(keys(sequences))
        sequences[i+1] = sequence
    end
end

function withsequences(f, session_id)
    lock(__sequences__[session_id]) do
        sequences = __sequences__[session_id]
        return f(sequences)
    end
end

## persistent storage

function exportsequence(session_id; appname="muscles")
    sequence = getsequence(session_id)
    isempty(sequence.program_name) && return ""
    dir = getbucket(session_id, appname, :write)
    filepath = joinpath(dir, sequence.program_name * ".json")
    MuscleActivities.to_json_file(filepath, sequence)
    return filepath
end

function loadsequence(session_id, filepath)
    sequence = MuscleActivities.from_json_file(filepath)
    addsequence(session_id, sequence)
    # NO! Let getsequence do the job
    # model = getmodel(session_id)
    # model.sequence[] = sequence
    return sequence
end

function deletesequence(session_id)
    model = getmodel(session_id)
    current = model.sequence[].program_name
    sequences = __sequences__[session_id]
    lock(sequences) do
        for (ix, seq) in pairs(sequences)
            if seq.program_name == current
                current = ix
                break
            end
        end
        @assert current isa Int
        pop!(sequences.cache, current)
    end
end

function purgesession(session_id; appname="muscles")
   model = lock(__backbone_cache__) do
        if session_id in keys(__backbone_cache__.cache)
            #pop!(__valid_sessions__, session_id) # not sure what to do with it
            pop!(__backbone_cache__.cache, session_id)
        end
    end
    lock(__sequences__) do
        if session_id in keys(__sequences__.cache)
            pop!(__sequences__.cache, session_id)
        end
    end
    purge_appdata(session_id, appname)
    return model
end

end
