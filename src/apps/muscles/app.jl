module MuscleApp

using NyxUI, NyxUI.Storage, NyxUI.GenieExtras
using Observables
using Genie, GenieSession, Stipple, StippleUI
import Stipple: @app, @init, @private, @in, @out, @onchange, @onbutton, @notify

include("Backbone.jl")
using .Backbone

export muscle_view

const MuscleWidgets = Backbone.MuscleWidgets

const default_colormap = MuscleWidgets.__default_colormap__

const colormaps = MuscleWidgets.__colormaps__

const maxlength = getconfig("muscle-activity", "maxlength")

const bonito_app = NamedApp(:inherit, Backbone.app)

@app begin
    @private bonito_session_id = ""

    @private sequence = MuscleActivity("")
    @in new_sequence_click = false
    @in new_sequence_type = "Empty"
    @in make_new_sequences_random = false
    @out sequence_names = String[]
    @in selected_sequence_name = ""
    @out selected_sequence_index = 0
    @in sequence_name = ""

    @out export_sequence_link = ""
    @in export_sequence_click = false

    @in colormap = default_colormap
    @out first_time_in_editmode = true # and in the heatmap view
    @in editmode = false
    @out editmode_disable = true
    @in series_length::Float64 = 21
    @in time_interval = 0.05
    @in start_time = 0.0

    @out heatmap_view = false

    @out area_selected = false
    @out new_clipboard_item = false

    @in delete_sequence_click = false
    @out no_sequences_yet = true

    @in reset_bonito_session = false

    @onchange isready begin
        _, url = init_model(__model__)
        run(__model__, """
        let iframe = document.getElementById('bonito'),
            loader = document.getElementById('bonito-loader');
        iframe.addEventListener('load', () => { loader.style.display = 'none'; });
        loader.style.display = 'block';
        iframe.src = '$url';
        """)
        session_id = bonito_session_id
        model = getmodel(session_id)
        if true # TODO: diagnose missing Stipple initialization
            # restore state after page reload (Ctrl+R)
            notify(sequence_names)
            selected_sequence_name = name = getsequence(session_id).program_name
            selected_sequence_index = @something findfirst(==(name), sequence_names[!]) 0
            colormap = model.colormap[]
            editmode = model.editmode[]
            sequence_name = name
            seq = model.sequence[]
            series_length = length(seq.times)
            time_interval = step(seq.times)
            start_time = first(seq.times)
            no_sequences_yet = isempty(sequence_names)
        end
        on(MuscleWidgets.clipboard(model)) do _
            new_clipboard_item[!] = false
            new_clipboard_item = true
        end
        on(MuscleWidgets.areaselect(model)) do _
            area_selected[!] = false
            area_selected = true
        end
        on(model.animation.switchlayer) do layers
            if !isnothing(layers)
                current = layers[1]
                heatmap_view = 1 < current
            end
        end
    end

    @onbutton reset_bonito_session begin
        model = purgesession(bonito_session_id; appname=bonito_app.name)
        # reset UI to defaults
        #bonito_session_id[!] = ""
        sequence[!] = MuscleActivity("")
        new_sequence_type[!] = "Empty"
        make_new_sequences_random[!] = false
        sequence_names[!] = String[]
        selected_sequence_name[!] = ""
        selected_sequence_index[!] = 0
        sequence_name[!] = ""
        export_sequence_link[!] = ""
        colormap[!] = default_colormap
        first_time_in_editmode[!] = true
        editmode[!] = false
        series_length[!] = 21
        time_interval[!] = 0.05
        start_time[!] = 0.0
        heatmap_view[!] = false
        no_sequences_yet[!] = true
        # reload page
        setmodel(bonito_session_id, sequence)
        run(__model__, "location.reload(true);")
    end

    @onchange start_time, time_interval, series_length begin
        model = getmodel(bonito_session_id)
        seq = model.sequence[]
        # input validation
        valid = true
        if time_interval <= 0
            if time_interval < 0
                # do not trigger on 0, because editing the corresponding field often implies
                # typing "0." first
                @notify "Time interval must be strictly positive" :error
                time_interval = step(seq.times)
            end
            valid = false
        end
        previous_length = length(seq.times)
        new_length = round(Int, series_length)
        if series_length < 1
            @notify "Time series length must be strictly positive" :error
            series_length = previous_length
            valid = false
        elseif series_length != new_length
            @notify "Time series length must be an integer" :error
            series_length = new_length
            valid = false
        elseif !isnothing(maxlength) && maxlength < new_length
            @notify "Time series is too long" :error
            series_length = previous_length
            valid = false
        elseif new_length < previous_length &&
            startswith(string(previous_length), string(new_length))
            @debug "Ignoring new size at the moment"
            valid = false
        end
        #
        if valid && !(start_time == seq.times[1] && time_interval == step(seq.times) &&
                      new_length == previous_length)
            stop_time = start_time + (new_length - 1) * time_interval
            ts = start_time:time_interval:stop_time
            @info "Resampling" start_time time_interval series_length
            MuscleActivities.resample!(seq, ts)
            notify(model.sequence)
            sequence[!] = seq
        end
    end

    @onbutton new_sequence_click begin
        sequence = newsequence(bonito_session_id, new_sequence_type, sequence_names,
                               start_time, time_interval, round(Int, series_length))
        # propagate
        name = sequence.program_name
        push!(sequence_names, name)
        selected_sequence_name[!] = sequence_name[!] = name
        selected_sequence_index[!] = length(sequence_names)
        start_time[!] = sequence.times[1]
        time_interval[!] = step(sequence.times)
        series_length[!] = length(sequence.times)
        # cannot simply loop
        notify(sequence_names)
        notify(selected_sequence_name)
        notify(sequence_name)
        notify(start_time)
        notify(time_interval)
        notify(series_length)
        # enable the download and delete buttons
        no_sequences_yet = false
        # toggle edit mode on
        editmode = true
    end

    @onchange make_new_sequences_random begin
        new_sequence_type = make_new_sequences_random ? "Random" : "Empty"
    end

    @onchange selected_sequence_name begin
        isempty(selected_sequence_name) && return
        seq = getsequence(bonito_session_id, selected_sequence_name)
        if !isnothing(seq)
            ix = findfirst(==(selected_sequence_name), sequence_names)
            selected_sequence_index[!] = ix
            sequence[!] = seq
            sequence_name[!] = seq.program_name
            start_time[!] = seq.times[1]
            time_interval[!] = step(sequence.times)
            series_length[!] = length(seq.times)
            #
            notify(sequence_name)
            notify(start_time)
            notify(series_length)
            notify(time_interval)
        end
    end

    @onchange sequence_name begin
        sequence[!].program_name == sequence_name && return
        # TODO: validate sequence_name, considering it may become a filename and be
        # interpolated in javascript
        valid = true
        windows_extra = "|:*?<>"
        for c in "./\\'\"`" * windows_extra
            if c in sequence_name
                valid = false
                break
            end
        end
        for nonprintable in 0x0:0x31
            if nonprintable in sequence_name
                valid = false
                break
            end
        end
        model = getmodel(bonito_session_id)
        if valid
            model.sequence[].program_name = sequence[!].program_name = sequence_name
            sequence_names[!][selected_sequence_index] = sequence_name
            notify(sequence_names)
        else
            @notify "Motor program name contains prohibited characters" :error
            sequence_name = sequence[!].program_name
        end
    end

    @onchange colormap begin
        model = getmodel(bonito_session_id)
        model.colormap[] = colormap
    end

    @onchange editmode begin
        if editmode && isempty(sequence_name)
            @notify "Create a new program or load one first" :error
            editmode = false
            return
        end
        model = getmodel(bonito_session_id)
        model.editmode[] = editmode
        editmode_disable = !editmode
        if editmode && first_time_in_editmode && heatmap_view
            first_time_in_editmode = false
        end
    end

    @onchange heatmap_view begin
        if heatmap_view && editmode && first_time_in_editmode
            first_time_in_editmode = false
        end
    end

    @onchange first_time_in_editmode begin
        @assert !first_time_in_editmode
        @notify "Toggle the \"edit mode\" off to browse the sequence with the heatmap" :info
    end

    @onbutton export_sequence_click begin
        filepath = exportsequence(bonito_session_id; appname=bonito_app.name)
        if isempty(filepath)
            @notify "No available motor programs; create one first" :error
        else
            filename = basename(filepath)
            appname = bonito_app.name
            mkpath("./public/$bonito_session_id/$appname")
            open("./public/$bonito_session_id/$appname/$filename", "w") do o
                open(filepath, "r") do i
                    write(o, read(i))
                end
            end
            export_sequence_link = "/$bonito_session_id/$appname/$filename"
        end
    end

    @onchange export_sequence_link begin
        filename = basename(export_sequence_link)
        run(__model__, """
        let a = document.createElement('a');
        a.setAttribute('href', '$export_sequence_link');
        a.setAttribute('download', '$filename');
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
        """)
    end

    @onchange fileuploads begin
        if !isempty(fileuploads)
            file = fileuploads["path"]
            try
                sequence = loadsequence(bonito_session_id, file)
                if !(sequence.program_name in sequence_names)
                    push!(sequence_names, sequence.program_name)
                end
                notify(sequence_names)
                selected_sequence_name = sequence.program_name
                # enable the download and delete buttons
                no_sequences_yet = false
            catch exception
                @error  "Failed to load file" exception
                @notify "Failed to load file" :error
            end
            # reset
            fileuploads = empty!(fileuploads)
        end
    end

    @onchange new_clipboard_item begin
        @notify  "Selection copied! Press Ctrl while clicking to paste" :info
    end
    @onchange area_selected begin
        @notify "Click inside the selected area to assign a value, or press Ctrl while clicking to copy the selection" :info
    end

    @onbutton delete_sequence_click begin
        isempty(selected_sequence_name) && return
        # update the model
        deletesequence(bonito_session_id)
        popat!(sequence_names, selected_sequence_index)
        if length(sequence_names) < selected_sequence_index
            selected_sequence_index[!] -= 1
        end
        if selected_sequence_index == 0
            n = round(Int, series_length)
            stop_time = start_time + (n-1) * time_interval
            time_support = start_time:time_interval:stop_time
            model = getmodel(bonito_session_id)
            muscles = model.overview
            sequence = MuscleActivity("", time_support, muscles)
            # update the view
            selected_sequence_name = ""
            # update the view
            sequence_name = ""
            # disable the download and delete buttons (view)
            no_sequences_yet = true
            # update the model
            model.sequence[] = sequence
        else
            # update both the view and model
            selected_sequence_name = sequence_names[selected_sequence_index]
        end
        # update the view
        notify(sequence_names)
    end

    @onchange no_sequences_yet begin
        if no_sequences_yet && editmode
            editmode = false
        end
    end
end

function dropdown(key, options, label; margin=true, class=nothing)
    dd = Html.select(key, options=options, label=label, useinput=true, class=class)
    if margin
        dd = item(dd)
    end
    return dd
end

function muscle_view(bonito_url=""; channel=":channel")
    col = "display:flex;flex-direction:column;"
    row = "display:flex;flex-direction:row;"
    widget = "width:1000px;height:900px;border:none;"

    topbar = Html.div(style=row, [
        item(uploader(multiple=true, accept=".json", maxfilesize=2^20, maxfiles=10,
                      label="Upload motor program(s)", autoupload=true, hideuploadbtn=true,
                      class="no-file-listing", id="uploader", url="/____/upload/$channel")),
        item(btn("Download motor program", @click(:export_sequence_click), color="primary",
                 loading=:export_sequence_click, disable=:no_sequences_yet,
                 [tooltip("Download the current motor program as a JSON file")])),
        dropdown(:selected_sequence_name, :sequence_names, "Motor program",
                 class="sequence-list"),
        item(btn("New", @click(:new_sequence_click), color="primary",
                 [tooltip("Create a new motor program with current parameters")])),
        checkbox("random", :make_new_sequences_random,
                 [tooltip("Make the next new motor program random")]),
    ])

    sidepanel = Html.div(style=col * "margin-left:4rem;", [
        dropdown(:colormap, colormaps, "Colormap"; margin=false),
        Html.div(class="edit-panel", style=col, [
            toggle("Toggle edit mode", :editmode, color="blue"),
            textfield("Motor program name", :sequence_name, disable=:editmode_disable),
            textfield("Number of time steps", :series_length, type="number", step="1",
                      disable=:editmode_disable),
            textfield("Time interval", :time_interval, type="number", step="any",
                      disable=:editmode_disable),
            textfield("Start time", :start_time, type="number", step="any",
                      disable=:editmode_disable),
        ]),
        item(btn("Delete motor program", @click(:delete_sequence_click), color="negative",
                 disable=:no_sequences_yet)),
    ])

    footer = Html.div([
        Html.span("Reset session", @click(:reset_bonito_session), style="cursor:pointer;",
                  [tooltip("Useful when the app crashes. All data will be lost!")]),
        Html.span(appinfo()),
    ]; id="footer", style="width:1354.617px;")

    Html.div(style=col, [
        topbar,
        Html.div(style=row, [
            Html.div(style=widget * "position:relative;display:block;", id="bonito-area", [
                Html.div(id="bonito-loader", class="loader", style="display:none;"),
                Html.iframe(style=widget * "position:absolute;top:0;", id="bonito",
                            src=bonito_url),
            ]),
            sidepanel,
        ]),
        footer,
    ])
end

const ui = muscle_view

function handler(modelview)
    muscle_model = @init
    url = ""
    #muscle_model, url = init_model()
    Stipple.Layout.add_css("/css/muscleapp.css")
    channel = muscle_model.channel__
    modelview(muscle_model, muscle_view(url; channel=channel))
end

function init_model(muscle_model=nothing)
    if isnothing(muscle_model)
        muscle_model = @init
        #@show muscle_model
    end
    genie_session = GenieSession.session()
    session_id = genie_session.id[1:32]
    if hasmodel(session_id)
        withsequences(session_id) do sequences
            ids = sort!(collect(keys(sequences)))
            muscle_model.sequence_names[!] = [sequences[i].program_name for i in ids]
        end
        muscle_model.sequence[!] = getmodel(session_id).sequence[]
    else
        setmodel(session_id, muscle_model.sequence[!])
    end
    BonitoServer.addsession(bonito_app, session_id)
    muscle_model.bonito_session_id[!] = session_id
    url = bonito_url(bonito_app, session_id)
    return muscle_model, url
end

end
