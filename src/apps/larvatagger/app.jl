module LarvaTagger

using NyxUI, NyxUI.Storage, NyxUI.GenieExtras
using NyxWidgets.Base: Cache
import LarvaTagger as LT
using GenieSession, Stipple
import Stipple: @app, @init, @private, @in, @onchange, @onbutton, @click
import StippleUI: tooltip

const sizefactors = Dict("1.0"=>1., "1.5"=>1.5, "2.0"=>2.)
const sizefactors_str = Dict(v=>k for (k,v) in pairs(sizefactors))
const appheight = Dict("1.0"=>"900px", "1.5"=>"1300px", "2.0"=>"1700px")
const appwidth = Dict("1.0"=>"1680px", "1.5"=>"2280px", "2.0"=>"2880px")

mutable struct Model
    sizefactor
    app # TODO: check whether explicitly closing apps helps
    appdata
end

const bonito_models = Cache{String, Model}()

const bonito_app = NamedApp("larvatagger",
    function (session)
        bucket = getbucket(session, "larvatagger", :read)
        mkpath(bucket)
        model = lock(bonito_models) do
            if haskey(bonito_models, session)
                bonito_models.cache[session]
            else
                inputfile = Ref{Union{Nothing, String}}(nothing)
                bonito_models.cache[session] = Model(1.0, nothing, inputfile)
            end
        end
        exportdir = joinpath("public", session, "larvatagger")
        function prepare_download(srcfile)
            mkpath(exportdir)
            filename = basename(srcfile)
            tempfile = joinpath(exportdir, filename)
            if !samefile(srcfile, tempfile)
                open(tempfile, "w") do fout
                    open(srcfile, "r") do fin
                        write(fout, read(fin))
                    end
                end
            end
            return "/$session/larvatagger/$filename"
        end
        isnothing(model.app) || close(model.app)
        model.app = app = LT.larvaeditor(model.appdata;
                                         root_directory=bucket,
                                         enable_uploads=true,
                                         enable_downloads=true,
                                         prepare_download=prepare_download,
                                         enable_new_directories=true,
                                         enable_delete=true,
                                         viewfactor=model.sizefactor)
        return app
    end
)

function purgesession(session)
    model = lock(bonito_models) do
        pop!(bonito_models.cache, session)
    end
    isnothing(model.app) || close(model.app)
    purge_appdata(session, "larvatagger")
    BonitoServer.addsession(bonito_app, session; restart=true)
end


@app begin
    @private bonito_session_id = ""
    @in sizefactor = "1.0"
    @in reset_bonito_session = false

    @onchange isready begin
        genie_session = GenieSession.session()
        bonito_session_id = genie_session.id[1:32]
        if haskey(bonito_models, bonito_session_id)
            sizefactor = sizefactors_str[bonito_models[bonito_session_id].sizefactor]
        else
            BonitoServer.addsession(bonito_app, bonito_session_id)
        end
        url = bonito_url(bonito_app, bonito_session_id)
        width = appwidth[sizefactor]
        height = appheight[sizefactor]
        run(__model__, """
            const iframe = document.getElementById('bonito');
            iframe.src = '$url';
            iframe.style.height = '$height';
            document.getElementById('footer').style.width = '$width';
            document.getElementById('iframe_alt_text').style.display = 'none';
        """)
    end

    @onchange sizefactor begin
        new_sizefactor = sizefactors[sizefactor]
        if new_sizefactor != bonito_models[bonito_session_id].sizefactor
            @info "New size factor; refreshing the iframe"
            bonito_models[bonito_session_id].sizefactor = new_sizefactor
            BonitoServer.addsession(bonito_app, bonito_session_id; restart=true) # restart
            width = appwidth[sizefactor]
            height = appheight[sizefactor]
            run(__model__, """
                const iframe = document.getElementById('bonito');
                iframe.style.height = '$height';
                iframe.src += '';
                document.getElementById('footer').style.width = '$width';
            """)
        end
    end

    @onbutton reset_bonito_session begin
        purgesession(bonito_session_id)
        # reset UI to defaults
        bonito_session_id[!] = ""
        sizefactor[!] = "1.0"
        # reload page
        run(__model__, "location.reload(true);")
    end
end

function view()
    [
        Html.div("Refresh the page if the present message does not disappear after a few seconds";
                 id="iframe_alt_text",
                 style="width:$(appwidth["1.0"]);margin:auto;padding:2rem;"),
        Html.iframe(; id="bonito", style="width:100%;height:0;border:none;"),
        Html.div([
            Html.div([
                Html.span("2D view size factor:&nbsp;"; style="margin-right:0.5rem;"),
                Html.select(:sizefactor; options=collect(keys(sizefactors)),
                            multiple=false, clearable=false, counter=false, usechips=false,
                            dense=true, var"options-dense"=true)];
                style="display:flex;flex-direction:horizontal;align-items:center;"),
            Html.span("Reset session", @click(:reset_bonito_session),
                      style="cursor:pointer;",
                      [tooltip("Useful when the app crashes. All data will be lost!")]),
            Html.span(appinfo())];
            id="footer",
        ),
    ]
end

function appinfo()
    version = string(pkgversion(LT))
    if endswith(version, ".0")
        version = version[1:end-2]
    end
    versionfile = joinpath(@__DIR__, "version.txt")
    if isfile(versionfile)
        version = join((version, readchomp(versionfile)), "-")
    end
    tagurl = "https://gitlab.pasteur.fr/nyx/LarvaTagger.jl/-/tags"
    return "LarvaTagger.jl <a href=\"$tagurl\">v$version</a>"
end

function handler(_)
    model = @init
    page(model, view; title="LarvaTagger")
end

end
