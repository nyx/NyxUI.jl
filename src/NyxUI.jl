module NyxUI

include("Storage.jl")
include("MuscleActivities.jl")
include("BonitoServer.jl")
include("GenieExtras.jl")

using .Storage
using .MuscleActivities
using .BonitoServer
using .GenieExtras

export MuscleActivities, MuscleActivity, BonitoServer, bonito_url, NamedApp, publish_css

end
