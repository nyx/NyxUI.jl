module MuscleActivities

using StructTypes
using NyxWidgets.Muscles
using Random
using Dates
using JSON3
using StructTypes
using OrderedCollections: OrderedDict
using ..Storage

export MuscleActivity, HalfSegmentActivity, resample!

const muscles = ("DA1", "DA2", "DA3", "DO1", "DO2", "DO3", "DO4", "DO5", "DT1", "LT4",
                 "LT3", "LT2", "LT1", "LL1", "LO1", "SBM", "VL1", "VL2", "VL3", "VL4",
                 "VO1", "VO2", "VO3", "VT1", "VA1", "VA2", "VA3", "VO6", "VO5", "VO4")

mutable struct HalfSegmentActivity
    times
    muscles::Vector{String}
    activity::Union{Nothing, Matrix{Float64}}
end

function HalfSegmentActivity(times, segment::MuscleWidget)
    segment_muscles = [m.name for (m, _) in Muscles.muscles(segment)]
    segment_muscles = [m for m in muscles if m in segment_muscles]
    if segment.upsidedown
        reverse!(segment_muscles)
    end
    HalfSegmentActivity(times, segment_muscles, nothing)
end

Base.size(seq::HalfSegmentActivity) = (length(seq.muscles), length(seq.times))

function Base.convert(::Type{Matrix{Float64}}, seq::HalfSegmentActivity)
    if isnothing(seq.activity)
        fill(NaN, size(seq))
    else
        seq.activity
    end
end

Base.convert(::Type{Matrix}, seq::HalfSegmentActivity) = convert(Matrix{Float64}, seq)


function Base.getindex(seq::HalfSegmentActivity, muscle::Muscle, step::Integer)
    seq[muscle.name, step]
end

function Base.getindex(seq::HalfSegmentActivity, muscle::AbstractString, step::Integer)
    if isnothing(seq.activity)
        NaN
    else
        m = findfirst(==(muscle), seq.muscles)
        if isnothing(m)
            @error "Muscle not found" muscle seq.muscles
            NaN
        else
            seq.activity[m, step]
        end
    end
end

function Base.setindex!(seq::HalfSegmentActivity, val, muscle::AbstractString, step)
    m = findfirst(==(muscle), seq.muscles)
    if isnothing(m)
        @error "Muscle not found" muscle seq.muscles
    else
        if isnothing(seq.activity)
            seq.activity = fill(NaN, length(seq.muscles), length(seq.times))
        end
        seq.activity[m, step] .= val
    end
end

mutable struct MuscleActivity
    program_name
    times
    halfsegments::OrderedDict{Tuple{String, String}, HalfSegmentActivity}
end

function MuscleActivity(program_name, times,
        muscles::MultiSegmentMuscleWidget=MultiSegmentMuscleWidget())
    halfsegments = OrderedDict((s.segment, s.side) => HalfSegmentActivity(times, s)
                               for s in segments(muscles))
    return MuscleActivity(program_name, times, halfsegments)
end

function MuscleActivity(program_name,
        muscles::MultiSegmentMuscleWidget=MultiSegmentMuscleWidget())
    MuscleActivity(program_name, 0:.05:1, muscles)
end

function Base.getindex(seq::MuscleActivity, seg::AbstractString,
        side::Union{Nothing, <:AbstractString})
    return seq.halfsegments[(seg, side)]
end

Base.getindex(seq::MuscleActivity, seg::MuscleWidget) = seq[seg.segment, seg.side]

function Base.getindex(seq::MuscleActivity, muscle::Muscle, step::Integer)
    seq[muscle.segment, muscle.side][muscle, step]
end

function rand!(seq::HalfSegmentActivity)
    rand() < .1 && return
    restricted_amplitude = rand() < 1/3
    seq.activity = heatmap = fill(NaN, size(seq))
    n = length(seq.times)
    muscles = Random.randperm(length(seq.muscles))
    for muscle in muscles[1:rand(0:end)]
        start = stop = 0
        while stop - start < 3
            start, stop = minmax(rand(1:n), rand(1:n))
        end
        series = rand(stop - start + 1)
        if restricted_amplitude
            # to test color scaling
            series = @. series / 3 + 1/3
        end
        series = @. round(series; digits=2)
        heatmap[muscle, start:stop] = series
    end
    return seq
end

function rand!(seq::MuscleActivity)
    foreach(rand!, values(seq.halfsegments))
    return seq
end

function resample!(sequence::MuscleActivity, timesupport)
    foreach(values(sequence.halfsegments)) do halfsegment
        resample!(halfsegment, timesupport)
    end
    sequence.times = timesupport
    return sequence
end

function resample!(sequence::HalfSegmentActivity, timesupport)
    if !isnothing(sequence.activity)
        m = size(sequence.activity, 2)
        n = length(timesupport)
        if n < m
            sequence.activity = sequence.activity[:, 1:n]
        elseif m < n
            padding = fill(NaN, size(sequence.activity, 1), n - m)
            sequence.activity = hcat(sequence.activity, padding)
        end
    end
    sequence.times = timesupport
    return sequence
end

## serialization

bysegment(seq::MuscleActivity) = bysegment(seq.halfsegments)

function bysegment(halves::AbstractDict)
    segments = unique(collect(segment for (segment, _) in keys(halves)))
    reordered = OrderedDict(segment => Tuple{String, HalfSegmentActivity}[]
                            for segment in segments)
    for ((segment, side), halfsegment) in pairs(halves)
        #if !isnothing(halfsegment.activity)
            push!(reordered[segment], (side, halfsegment))
        #end
    end
    return reordered
end

function contiguous(signal, i)
    series = Tuple{Int, Vector{Float64}}[]
    if !isnothing(signal)
        signal = signal[i, :]
        start = findfirst(!isnan, signal)
        while !isnothing(start)
            stop = findfirst(isnan, signal[start:end])
            subseries = if isnothing(stop)
                signal[start:end]
            else
                stop += start - 2
                signal[start:stop]
            end
            push!(series, (start, subseries))
            if isnothing(stop)
                start = nothing
            else
                start = findfirst(!isnan, signal[stop+1:end])
                if !isnothing(start)
                    start += stop
                end
            end
        end
    end
    return series
end

function to_json_file(filepath::String, object)
    session_bucket = getbucket(child_resource=filepath)
    @assert isdir(session_bucket)
    records = length(readdir(session_bucket))
    maxfiles = getconfig("per-session", "maxfiles")
    if !(isnothing(maxfiles) || records <= maxfiles)
        clear_oldest(session_bucket)
    end
    #
    open(filepath, "w") do f
        write_json(f, object)
    end
end

write_json(f, object) = JSON3.pretty(f, JSON3.write(object))

from_json_file(filepath::String) = from_json_file(MuscleActivity, filepath)

from_json_file(::Type{T}, filepath) where {T} = JSON3.read(read(filepath, String), T)

StructTypes.StructType(::Type{MuscleActivity}) = StructTypes.CustomStruct()

function StructTypes.lower(seq::MuscleActivity)
    version = string(pkgversion(@__MODULE__))
    if endswith(version, ".0")
        version = version[1:end-2]
    end
    datetime = Dates.format(Dates.now(), "yyyymmdd_HHMMSS")
    Dict = OrderedDict
    Dict("name" => seq.program_name,
         "metadata" =>
         Dict("software" =>
              Dict("name" => "NyxUI",
                   "version" => version),
              "date_time" => datetime),
         "time" =>
         Dict("start" => seq.times[1],
              "step" => step(seq.times),
              "length" => length(seq.times)),
         "segments" =>
         Dict(segment =>
              [Dict("muscle" => muscle,
                    "side" => side,
                    "activity" =>
                    [Dict("start" => start,
                          "series" => series)
                     for (start, series) in contiguous(half.activity, i)])
               for (side, half) in halves
               for (i, muscle) in enumerate(half.muscles)]
              for (segment, halves) in bysegment(seq)))
end

function StructTypes.construct(::Type{MuscleActivity}, dict)
    Dict = OrderedDict
    program_name = dict["name"]
    times = dict["time"]
    times = range(start=times["start"], step=times["step"], length=times["length"])
    halves = Dict{Tuple{String, String}, HalfSegmentActivity}()
    for (segment, muscles) in pairs(dict["segments"])
        musclenames = Dict{String, Vector{String}}()
        for muscle in muscles
            push!(get!(musclenames, muscle["side"], String[]), muscle["muscle"])
        end
        activity = Dict(side => fill(NaN, length(musclenames[side]), length(times))
                           for side in keys(musclenames))
        for muscle in muscles
            if !isempty(muscle["activity"])
                name, side = muscle["muscle"], muscle["side"]
                row = findfirst(==(name), musclenames[side])
                for series in muscle["activity"]
                    col, x = series["start"], series["series"]
                    activity[side][row, col:col+length(x)-1] = x
                end
            end
        end
        for side in keys(musclenames)
            names, x = musclenames[side], activity[side]
            halves[(segment, side)] = HalfSegmentActivity(times, names, x)
        end
    end
    return MuscleActivity(program_name, times, halves)
end

end
