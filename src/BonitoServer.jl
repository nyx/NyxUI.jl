module BonitoServer

using Bonito

export bonitoserver, bonito_url, addsession, NamedApp

## app

mutable struct NamedApp
    name
    app
end

function name(named_app::NamedApp)
    name = named_app.name
    if name === :inherit
        name = ""
    end
    return name
end

## server

const __server__ = Ref{Union{Nothing, Bonito.Server}}(nothing)

function bonitoserver(; port=nothing, kwargs...)
    server = __server__[]
    if isnothing(server)
        if isnothing(port)
            port = get(ENV, "BONITO_PORT", 9285)
        end
        if :proxy_url ∉ keys(kwargs)
            if "BONITO_BASE_PATH" in keys(ENV)
                kwargs = Dict{Symbol,Any}(kwargs)
                kwargs[:proxy_url] = ENV["BONITO_BASE_PATH"]
                @info "Setting proxy url" ENV["BONITO_BASE_PATH"]
            end
        end
        __server__[] = server = Server("127.0.0.1", port; kwargs...)
    end
    return server
end

route(named_app::NamedApp, session, args...) = route(session, name(named_app), args...)

function route(session::AbstractString, suffix::AbstractString="",
        prefix::AbstractString="sessions")
    parts = [""]
    for part in (prefix, session, suffix)
        isempty(part) || push!(parts, part)
    end
    if length(parts) == 1
        error("cannot define route; arguments `session`, `suffix` and `prefix` are all empty")
    end
    return join(parts, "/")
end

function addsession(args...; restart=false, kwargs...)
    addsession(bonitoserver(; kwargs...), args...; restart=restart)
end

function addsession(server::Bonito.Server, named_app, session_id; restart=false)
    app_name = name(named_app)
    app_route = route(session_id, app_name)
    if restart || !Bonito.has_route(server.routes, app_route)
        if Bonito.has_route(server.routes, app_route)
            @info "New handler" session_id app_name
        else
            @info "New user session" session_id app_name
        end
        app = named_app.app
        Bonito.route!(server, app_route => app(session_id))
        Bonito.HTTPServer.start(server)
    end
end

bonito_url(args...; kwargs...) = bonito_url(bonitoserver(; kwargs...), args...)

function bonito_url(server::Bonito.Server, args...)
    Bonito.HTTPServer.online_url(server, route(args...))
end

end
