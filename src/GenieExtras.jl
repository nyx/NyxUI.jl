module GenieExtras

import Genie.Renderer.Html

export publish_css, appinfo, add_footer

const project_root = dirname(Base.current_project())

function publish_css(; clear=true)
    css_dir = joinpath(project_root, "public", "css")
    if clear && ispath(css_dir)
        rm(css_dir, recursive=true)
    end
    mkpath(css_dir)
    for (parent, _, files) in walkdir(@__DIR__)
        for file in files
            # note: assets for Bonito components are published elsewhere
            if endswith(file, ".css") && !startswith(file, "bonito")
                if isfile(joinpath(css_dir, file))
                    @warn "Multiple CSS files with same name" file
                end
                open(joinpath(parent, file), "r") do i
                    open(joinpath(css_dir, file), "w") do o
                        write(o, read(i))
                    end
                end
            end
        end
    end
    return css_dir
end

function appinfo()
    version = string(pkgversion(@__MODULE__))
    if endswith(version, ".0")
        version = version[1:end-2]
    end
    versionfile = joinpath(@__DIR__, "version.txt")
    if isfile(versionfile)
        version = join((version, readchomp(versionfile)), "-")
    end
    tagurl = "https://gitlab.pasteur.fr/nyx/NyxUI/-/tags"
    return "NyxUI.jl <a href=\"$tagurl\">v$version</a>"
end

footer() = Html.footer(appinfo())

add_footer(ui::AbstractString, footer=footer) = "$ui$(footer())"

add_footer(ui::Function, footer=footer) = () -> add_footer(ui(), footer)

end
