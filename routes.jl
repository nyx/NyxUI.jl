#!/usr/bin/env bash
#=
PROJECT_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")"; pwd -P)

if [ -z "$JULIA" ]; then JULIA=julia; fi

exec "$JULIA" --project="$PROJECT_DIR" --startup-file=no --color=yes\
    -iq "${BASH_SOURCE[0]}" $@
=#

using NyxUI
using Genie, GenieSession, Stipple, StippleUI

const nyxui_title = "Nyx UI"

const nyxui_css = "/css/nyxui.css"

publish_css()

base_path = get(ENV, "GENIE_BASE_PATH", "")

Genie.Configuration.config!(
    server_port=get(ENV, "GENIE_PORT", 9284),
    base_path=base_path,
    websockets_base_path=base_path,
    app_env=get(ENV, "GENIE_ENV", "dev"),
)

if Genie.Configuration.isprod()
    @error "GENIE_ENV=prod is not implemented"
end

function modelview(model, view)
    page(model, view; title=nyxui_title)
end

Stipple.Layout.add_css(nyxui_css)

include("src/apps/catalog/app.jl")

route("/") do
    AppCatalog.handler(modelview)
end

include("src/apps/muscles/app.jl")
MuscleApp.bonito_app.name = "muscles"

route("/muscles") do
    MuscleApp.handler(modelview)
end

include("src/apps/larvatagger/app.jl")
LarvaTagger.bonito_app.name = "larvatagger"

route("/larvatagger") do
    LarvaTagger.handler(modelview)
end

run_as_script = isinteractive()
up(; async=run_as_script)
#display(NyxUI.bonitoserver())

