#!/usr/bin/env bash

CONTAINERFILE=front/Containerfile.local

if ! [ -f "$CONTAINERFILE" -a -f routes.jl ]; then
  echo "Run $0 from the project root directory"
  exit 1
fi

IMAGE=nyx-dev

podman rmi -f $IMAGE || true

rm -rf ../LarvaTagger.jl; cp -Rp ../../LarvaTagger/LarvaTagger.jl ../

podman build --tag $IMAGE -f "$CONTAINERFILE" .. # --no-cache

#mkdir -p public

podman run -d -p 8081:80 -p 9484:9284 -p 9485:9285 \
  $IMAGE

CONTAINER=`podman ps | grep $IMAGE | cut -d' ' -f1`

if [ -n "$CONTAINER" ]; then
  sleep 1
  podman logs -f $CONTAINER
fi
