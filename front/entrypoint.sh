#!/usr/bin/env bash

set -m


/docker-entrypoint.sh nginx -g "daemon off;" &>/dev/null &
NGINX_PID=$!


echo "JULIA_PROJECT=$JULIA_PROJECT"

#export GENIE_ENV=prod
#export GENIE_BASE_PATH=/g
export BONITO_BASE_PATH=/bonito

cd "$JULIA_PROJECT"
julia --project=. routes.jl

kill $NGINX_PID
wait $NGINX_PID
