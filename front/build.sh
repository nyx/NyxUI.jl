#!/usr/bin/env bash

CONTAINERFILE=front/Containerfile

if ! [ -f "$CONTAINERFILE" -a -f routes.jl ]; then
  echo "Run $0 from the project root directory"
  exit 1
fi

IMAGE=nyx-prod

podman rmi -f $IMAGE || true

podman build --tag $IMAGE -f "$CONTAINERFILE" .

if [ "$1" = "--now" ]; then
  podman run -d -p 8080:8080 $IMAGE

  CONTAINER=`podman ps | grep $IMAGE | cut -d' ' -f1`

  if [ -n "$CONTAINER" ]; then
    sleep 1
    podman logs -f $CONTAINER
  fi
fi
